/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lharvey <lharvey@student.hive.fi>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/24 09:55:16 by lharvey           #+#    #+#             */
/*   Updated: 2022/11/16 16:01:09 by lharvey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	size_t	i;
	size_t	srclen;
	size_t	dstlen;

	i = 0;
	if (!dst)
		dstlen = 0;
	else
		dstlen = ft_strlen(dst);
	srclen = ft_strlen(src);
	if (dstsize < dstlen + 1)
		return (srclen + dstsize);
	while (i < (dstsize - dstlen - 1) && src[i])
	{
		dst[i + dstlen] = src[i];
		i++;
	}
	if (dstsize != 0 || dstlen <= dstsize)
		dst[i + dstlen] = '\0';
	return (srclen + dstlen);
}
