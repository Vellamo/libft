/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lharvey <lharvey@student.hive.fi>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 17:15:14 by lharvey           #+#    #+#             */
/*   Updated: 2022/11/22 13:35:18 by lharvey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_list	*ft_lstnew(void *content)
{
	t_list		*lstnew;

	lstnew = (t_list *)malloc(sizeof(t_list) * 1);
	if (!lstnew)
		return (0);
	if (!content)
		lstnew->content = 0;
	else
		lstnew->content = content;
	lstnew->next = 0;
	return (lstnew);
}
