/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lharvey <lharvey@student.hive.fi>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/31 16:57:49 by lharvey           #+#    #+#             */
/*   Updated: 2022/11/17 15:14:34 by lharvey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static int	ft_intlen(int n)
{
	int	intlen;

	if (n <= 0)
		intlen = 1;
	else
		intlen = 0;
	while (n != 0)
	{
		n /= 10;
		intlen++;
	}
	return (intlen);
}

static int	isnegative(int n)
{
	if (n < 0)
		return (1);
	else
		return (0);
}

static unsigned int	signageflip(int n)
{
	if (n < 0)
		return (-n);
	else
		return (n);
}

char	*ft_itoa(int n)
{
	char			*fresh;
	int				len;
	int				negative;
	unsigned int	num;

	len = ft_intlen(n);
	negative = isnegative(n);
	num = signageflip(n);
	fresh = (char *)malloc(((len) + 1) * (sizeof(char)));
	if (!fresh)
		return (0);
	fresh[len--] = '\0';
	while (len >= 0)
	{
		fresh[len] = (num % 10) + '0';
		num /= 10;
		len--;
	}
	if (negative == 1)
		fresh[0] = '-';
	return (fresh);
}
