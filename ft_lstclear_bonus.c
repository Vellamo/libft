/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear_bonus.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lharvey <lharvey@student.hive.fi>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/01 11:37:00 by lharvey           #+#    #+#             */
/*   Updated: 2022/11/22 13:35:04 by lharvey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void*))
{
	t_list	*ptr;
	t_list	*bhind;

	if (!del || !lst)
		return ;
	ptr = (t_list *)(*lst);
	bhind = ptr;
	while (ptr != NULL)
	{
		ptr = ptr->next;
		ft_lstdelone(bhind, del);
		bhind = ptr;
	}
	*lst = NULL;
}
