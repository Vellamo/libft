/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lharvey <lharvey@student.hive.fi>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/30 14:46:58 by lharvey           #+#    #+#             */
/*   Updated: 2022/11/29 16:51:01 by lharvey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static char	*join_strings(char const *s1, char const *s2)
{
	unsigned int	s1len;
	unsigned int	s2len;
	char			*string;

	s1len = ft_strlen(s1);
	s2len = ft_strlen(s2);
	string = (char *)malloc(((s1len + s2len) + 1) * (sizeof(char)));
	if (!string)
		return (NULL);
	ft_memcpy(string, s1, s1len);
	ft_memcpy(&string[s1len], s2, s2len);
	string[s1len + s2len] = '\0';
	return (string);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	if (!s1 && !s2)
		return (NULL);
	else if (!s1)
		return ((char *)s2);
	else if (!s2)
		return ((char *)s1);
	return (join_strings(s1, s2));
}
