#include <stdlib.h>

static int	ft_intlen_base(int n, int base)
{
	int	intlen;

	if (n <= 0)
		intlen = 1;
	else
		intlen = 0;
	while (n != 0)
	{
		n /= base;
		intlen++;
	}
	return (intlen);
}

static unsigned int	negative_test(int n, int *negative, int base)
{
	if (n < 0 && base == 10)
    {
        *negative = 1;
		return (-n);
    }
	else
    {
        *negative = 0;
		return (n);
    }
}

char	*ft_itoa_base(int n, int base)
{
	char			*fresh;
	int				len;
	int				negative;
	unsigned int	num;

	len = ft_intlen_base(n, base);
    num = negative_test(n, &negative, base);
    fresh = (char *)malloc(((len) + 1) * (sizeof(char)));
	if (!fresh)
		return (0);
	fresh[len--] = '\0';
	while (len >= 0)
	{
        if (base < 9)
        	fresh[len] = (num % base) + '0';
        else
            fresh[len] = ((num % base) - 10) + 'a';
		num /= base;
		len--;
	}
	if (negative == 1)
		fresh[0] = '-';
	return (fresh);
}
