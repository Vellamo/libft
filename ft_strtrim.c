/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lharvey <lharvey@student.hive.fi>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/31 11:59:05 by lharvey           #+#    #+#             */
/*   Updated: 2022/11/18 11:39:21 by lharvey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_trimfnd(const char *haystack, const char *needle)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	j = 0;
	if (*needle == '\0')
		return ((char *)haystack);
	while (haystack[i] != '\0')
	{
		while (haystack[i] != needle[j] && needle[j])
			j++;
		if (ft_strlen(needle) == j)
			return ((char *)(&(haystack[i])));
		j = 0;
		i++;
	}	
	return ((char *)&(haystack[i]));
}

static char	*ft_invtrimfnd(const char *haystack, const char *needle)
{
	int				i;
	unsigned int	j;

	i = ft_strlen(haystack) - 1;
	j = 0;
	if (*needle == '\0')
		return ((char *)haystack);
	while (i >= 0)
	{
		while (haystack[i] != needle[j] && needle[j])
			j++;
		if (ft_strlen(needle) == j)
			return ((char *)(&(haystack[i])));
		j = 0;
		i--;
	}
	if (i < 0)
		return ((char *)&(haystack[0]));
	return ((char *)&(haystack[i]));
}

char	*ft_strtrim(char const *s1, char const *set)
{
	char			*start;
	char			*end;
	char			*trimmed;

	if (!s1)
		return (NULL);
	if (!(*set))
		return (ft_strdup(s1));
	start = ft_trimfnd(s1, set);
	end = ft_invtrimfnd((char const *)start, set);
	if (*start == '\0' || *end == s1[0])
	{
		trimmed = (char *)ft_calloc((size_t)1, (size_t)1);
		trimmed[0] = '\0';
	}
	else
		trimmed = ft_substr(start, 0, (1 + \
		((ft_strlen(start)) - (ft_strlen(end)))));
	return (trimmed);
}
