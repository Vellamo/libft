/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lharvey <lharvey@student.hive.fi>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/01 11:27:44 by lharvey           #+#    #+#             */
/*   Updated: 2022/11/18 17:41:12 by lharvey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int	wordcnt(char const *search, char x)
{
	int	a;
	int	wordcount;

	a = 0;
	wordcount = 0;
	if (!x)
		return (wordcount);
	while (search[a] != '\0')
	{
		if (search[a] != x)
		{
			wordcount++;
			while (search[a] != x && search[a])
				a++;
		}
		while (search[a] == x)
			a++;
	}
	return (wordcount);
}

static int	cnt(const char *char_array, char h)
{
	int	b;

	b = 0;
	while ((char_array[b] != '\0') && (char_array[b] != h))
		b++;
	return (b);
}

static char	**ft_initialise(char const *s, char c)
{
	char		**fresh;

	if (!s)
		return (NULL);
	fresh = (char **)malloc((wordcnt(s, c) + 1) * (sizeof(char *)));
	if (!(fresh))
		return (NULL);
	return (fresh);
}

char	**ft_split(char const *s, char c)
{
	int			i;
	int			j;
	char		**fresh;

	i = 0;
	fresh = ft_initialise(s, c);
	if (!fresh)
		return (NULL);
	while (*s)
	{
		j = 0;
		if (*s != c && *s)
		{
			fresh[i] = (char *)malloc((cnt(&s[j], c) + 1));
			if (!(fresh[i]))
				return (NULL);
			while (*s != c && *s)
				fresh[i][j++] = (char)*s++;
			fresh[i++][j] = '\0';
		}
		while (*s == c && *s)
			s++;
	}
	fresh[i] = NULL;
	return (fresh);
}
