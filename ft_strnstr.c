/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lharvey <lharvey@student.hive.fi>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/24 17:29:24 by lharvey           #+#    #+#             */
/*   Updated: 2022/11/17 15:54:14 by lharvey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	i;
	char	*hay;
	char	*ned;
	size_t	nedlen;

	if (*needle == '\0')
		return ((char *)haystack);
	if (len == 0)
		return (NULL);
	i = 0;
	hay = (char *)haystack;
	ned = (char *)needle;
	nedlen = ft_strlen(ned);
	while (hay[i] != '\0' && (i + nedlen) <= len)
	{
		if (ft_memcmp(&(hay[i]), ned, nedlen) == 0)
		{
			return (&(hay[i]));
		}
		++i;
	}
	return (NULL);
}
