/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lharvey <lharvey@student.hive.fi>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/01 12:49:57 by lharvey           #+#    #+#             */
/*   Updated: 2022/11/29 15:31:25 by lharvey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>
#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	char	*temp;
	size_t	bytes;

	if (count == 0 || size == 0)
		return (ft_calloc(1, 1));
	bytes = count * size;
	if (bytes / size != count)
		return (NULL);
	temp = (void *)malloc(count * size);
	if (temp == NULL)
		return (NULL);
	ft_bzero(temp, count * size);
	return ((void *)temp);
}
